# The [@dril][dril] Manual of Style

This is the repository for the [@dril][dril] _Manual of Style_.
This work is intended to codify the writing style of the most iconic
prose stylist of our time[^1].

[^1]: [@awoke_hoover, 2018][stylist]
[dril]: https://twitter.com/dril “@dril”
[stylist]: https://twitter.com/woke_hoover/status/1037174993178767361 “@woke_hoover, 2018”
